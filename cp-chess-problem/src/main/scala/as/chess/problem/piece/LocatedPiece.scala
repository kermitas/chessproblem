package as.chess.problem.piece

case class LocatedPiece(x: Int, y: Int, piece: Piece) extends Serializable
